# Chameleon User Guide: Edge to Cloud
# This example is based on:
# https://www.chameleoncloud.org/experiment/share/37991779-fd7b-4ab0-8d6f-e726a9204946

from ipaddress import IPv6Interface
import logging
import re
import time
import enoslib as en
from enoslib.infra.enos_g5k.g5k_api_utils import get_api_client
from enoslib.infra.providers import Providers
import iotlabcli


logging.basicConfig(level=logging.DEBUG)
# Leasing resources on Grid5000
g5k_conf = {
    "walltime": "02:00:00",
    "job_name": "enoslib-grid-to-fit-lease",
    "job_type": "allow_classic_ssh",
    "resources": {
        "machines": [{
            "roles": ["server"],
            "cluster": "parasilo",
            "nodes": 1,
            "primary_network": "my_network",
            "secondary_network": [],
        }],
        "networks": [{
            "id": "my_network",
            "type": "prod",
            "roles": ["my_network"],
            "site": "rennes"}
        ]}
}
conf_g5k = en.G5kConf().from_dictionnary(g5k_conf)
provider_g5k = en.G5k(conf_g5k)

# Leasing resources on Fit
fit_conf = {
    "walltime": "02:00",
    "job_name": "enoslib-grid-to-fit-lease",
    "resources": {
        "machines": [
            {
                "roles": ["client"],
                "archi": "a8:at86rf231",
                "number": 1,
                "site": "grenoble"
            },
        ],
    }
}

fit_conf = en.IotlabConf().from_dictionary(fit_conf)
provider_fit = en.Iotlab(fit_conf)


providers = Providers([provider_g5k, provider_fit])
roles, networks = providers.init()

print("\nRéservation faite\n")

provider_g5k, provider_fit = providers.providers

en.run("dhclient -6 br0", roles=roles["G5k"])
r = en.run("ip a", roles["G5k"])

roles = en.sync_info(roles, networks)

g5k_server = roles["server"][0]
addresses = g5k_server.filter_addresses(networks=networks["my_network"])

floating_ip = [str(a.ip.ip) for a in addresses if isinstance(a.ip, IPv6Interface)][0]

other = roles["Iotlab"][0]

addresses = roles["Iotlab"][0].filter_addresses(networks=networks["Iotlab"])

ip_rpi = [str(a.ip.ip) for a in addresses if isinstance(a.ip, IPv6Interface)][0]

print(f"\nFloating ip {floating_ip}\n")
print(f"\nIp Iotlat {ip_rpi}\n")

logging.info('*' * 40 + f" roles{type(roles)} = {roles}")
logging.info('*' * 40 + f" networks{type(networks)} = {networks}")


# Experimentation logic starts here
# Grid'5000 Cloud
dest_dir = "/tmp/edge-to-cloud"
with en.play_on(roles=roles["server"]) as p:
    p.copy(src="./artifacts_cloud/",
           dest=dest_dir)
    p.shell(f"bash {dest_dir}/cloud_worker.sh > {dest_dir}/tests")
# FiT Edge

with en.play_on(roles=roles["client"]) as p:
    p.copy('./artifacts_edge/', '/')
    p.shell(f"bash /edge_worker.sh edge_data 100 {floating_ip}")


logging.info("Running experiment for 300 secs...")
time.sleep(300)
# How to check execution?
# ssh to the Cloud server using floating_ip: ssh cc@<floating_ip>
# tail -f /home/cc/predict.log
# you may also check mosquitto topic:
# mosquitto_sub -v -h 127.0.0.1 -p 1883 -t 'edge_data'


# Releasing resources from Chameleon Cloud and Edge
logging.info("Releasing resources.")
for p in providers:
    p.destroy()
