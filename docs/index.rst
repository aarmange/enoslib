.. enoslib documentation master file, created by
   sphinx-quickstart on Thu Sep 21 21:45:39 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

.. include:: ../README.rst

At a glance
-----------

The tip of the Iceberg featuring Grid'5000 testbed and a dummy benchmark.

.. raw:: html

   <script id="asciicast-zFHSJ2PkXnvI5K18vECdhRvgZ" src="https://asciinema.org/a/zFHSJ2PkXnvI5K18vECdhRvgZ.js" data-speed="2" data-rows="50" async></script>

.. toctree::
   :maxdepth: 2
   :hidden:
   :titlesonly:

   tutorials/index.rst
   apidoc/index.rst
   jupyter/index.rst
   theyuseit.rst
   changelog.rst
